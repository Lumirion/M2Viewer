# borrowed some code from https://github.com/Perlmint/irrlicht-cmake/blob/master/CMakeLists.txt
CMAKE_MINIMUM_REQUIRED( VERSION 2.6 )
PROJECT( irrlicht )

#SET( PROJECT_SOURCE_DIR "${CMAKE_SOURCE_DIR}/src/dep}" )
SET( IRR_SOURCES "${CMAKE_SOURCE_DIR}/src/dep/src/irrlicht" )
SET( IRR_SUB_DEPS "${CMAKE_SOURCE_DIR}/src/dep/src/irrlicht" )
SET( IRR_INCLUDES "${CMAKE_SOURCE_DIR}/src/dep/include/irrlicht" )
Set( EXTRAS "${CMAKE_SOURCE_DIR}/src/Viewer" )

# each call of include_directories apends its directories to the previous ones by default
INCLUDE_DIRECTORIES( "${IRR_INCLUDES}" "${IRR_SOURCES}" "${EXTRAS}" )


FILE( GLOB IRRLICHT_SRC_FILES "${IRR_SOURCES}/*.cpp" "${IRR_SOURCES}/*.c" "${IRR_SOURCES}/*.h" "${IRR_SOURCES}/*.hpp" )
LIST(APPEND IRRLICHT_SRC_FILES "${CMAKE_SOURCE_DIR}/src/Viewer/CM2Mesh.cpp")

FILE( GLOB AESGLADMAN_SRC_FILES "${IRR_SUB_DEPS}/aesGladman/*.cpp" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/aesGladman" )

FILE( GLOB BZIP2_SRC_FILES "${IRR_SUB_DEPS}/bzip2/*.c" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/bzip2" )
LIST( REMOVE_ITEM BZIP2_SRC_FILES "${IRR_SUB_DEPS}/bzip2/bzip2.c" "${IRR_SUB_DEPS}/bzip2/bzip2recover.c" "${IRR_SUB_DEPS}/bzip2/dlltest.c" "${IRR_SUB_DEPS}/bzip2/mk251.c" "${IRR_SUB_DEPS}/bzip2/spewG.c" "${IRR_SUB_DEPS}/bzip2/unzcrash.c" )

FILE( GLOB LZMA_SRC_FILES "${IRR_SUB_DEPS}/lzma/*.c" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/lzma" )

FILE( GLOB ZLIB_SRC_FILES "${IRR_SUB_DEPS}/zlib/*.c" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/zlib" )
LIST( REMOVE_ITEM ZLIB_SRC_FILES "${IRR_SUB_DEPS}/zlib/gzclose.c" "${IRR_SUB_DEPS}/zlib/gzlib.c" "${IRR_SUB_DEPS}/zlib/gzread.c" "${IRR_SUB_DEPS}/zlib/gzwrite.c" "${IRR_SUB_DEPS}/zlib/infback.c" )

FILE( GLOB JPEGLIB_SRC_FILES "${IRR_SUB_DEPS}/jpeglib/j*.c" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/jpeglib" )
LIST( REMOVE_ITEM JPEGLIB_SRC_FILES "${IRR_SUB_DEPS}/jpeglib/jmemdos.c" "${IRR_SUB_DEPS}/jpeglib/jmemmac.c" "${IRR_SUB_DEPS}/jpeglib/jmemname.c" "${IRR_SUB_DEPS}/jpeglib/jmemansi.c" "${IRR_SUB_DEPS}/jpeglib/jpegtran.c" )

FILE( GLOB LIBPNG_SRC_FILES "${IRR_SUB_DEPS}/libpng/*.c" )
INCLUDE_DIRECTORIES( "${IRR_SUB_DEPS}/libpng" )

#set(INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/src/dep/include ${PROJECT_SOURCE_DIR}/src/dep/include/irrlicht ${PROJECT_SOURCE_DIR}/src/dep/include/zlib ${PROJECT_SOURCE_DIR}/src/Viewer)

IF( APPLE )

	FILE( GLOB IRRLICHT_OSX_SRC_FILES "${IRR_SOURCES}/MacOSX/*.mm" "${IRR_SOURCES}/MacOSX/*.h" )
	LIST( APPEND IRRLICHT_SRC_FILES ${IRRLICHT_OSX_SRC_FILES} )
	INCLUDE_DIRECTORIES( "${IRR_SOURCES}/MacOSX" )

	FIND_LIBRARY( OPENGL_LIB OpenGL )
	FIND_LIBRARY( COCOA_LIB Cocoa )
	FIND_LIBRARY( CARBON_LIB Carbon )
	FIND_LIBRARY( IOKIT_LIB IOKit )

	LIST( APPEND IRRLICHT_LINK_LIBS ${OPENGL_LIB} ${IOKIT_LIB} ${COCOA_LIB} ${CARBON_LIB} )
	ADD_DEFINITIONS( -DMACOSX )

	SET_SOURCE_FILES_PROPERTIES("${IRR_SOURCES}/Irrlicht.cpp" "${IRR_SOURCES}/COpenGLDriver.cpp" PROPERTIES COMPILE_FLAGS "-x objective-c++")

ELSEIF( WIN32 )
  # note that WIN32 doesn't mean 32bit windows.  It means Windows in general.
    set(LIBTYPE SHARED)
    #set(INCLUDE_DIRS ${INCLUDE_DIRS} ${DirectX_D3D9_INCLUDE_DIR} ${DirectX_D3DX9_INCLUDE_DIR})
	
	# append additional includes for windows builds
	#INCLUDE_DIRECTORIES(${DirectX_D3D9_INCLUDE_DIR} ${DirectX_D3DX9_INCLUDE_DIR})
	# later, our finddirectx.cmake should be updated to look in windows sdk and the copy in 
	# psuwow sources.  Directx include directories can be merged into a single variable.
	# see https://github.com/Perlmint/irrlicht-cmake/blob/master/CMakeLists.txt#L68 
	ADD_DEFINITIONS( -D_USRDLL -DIRRLICHT_EXPORTS -D_CRT_SECURE_NO_DEPRECATE )
	INCLUDE_DIRECTORIES( ${DirectX_D3D9_INCLUDE_DIR} ${DirectX_D3DX9_INCLUDE_DIR} )
    LIST( APPEND IRRLICHT_LINK_LIBS ${DirectX_D3D9_LIBRARY} ${DirectX_D3DX9_LIBRARY} Winmm)
else()
    set(LIBTYPE STATIC)
endif()
#include_directories(${INCLUDE_DIRS})

SOURCE_GROUP( Irrlicht\\libraries\\aesGladman FILES ${AESGLADMAN_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\libraries\\bzip2 FILES ${BZIP2_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\libraries\\lzma FILES ${LZMA_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\libraries\\zlib FILES ${ZLIB_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\libraries\\jpeglib FILES ${JPEGLIB_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\libraries\\libpng FILES ${LIBPNG_SRC_FILES} )
SOURCE_GROUP( Irrlicht\\Engine FILES ${IRRLICHT_SRC_FILES} )

SET( IRRLICHT_LIB_FILES ${IRRLICHT_LIB_FILES} ${AESGLADMAN_SRC_FILES} ${BZIP2_SRC_FILES} ${LZMA_SRC_FILES} ${ZLIB_SRC_FILES} ${JPEGLIB_SRC_FILES} ${LIBPNG_SRC_FILES} )

# create a library of whatever type LIBTYPE was set to
ADD_LIBRARY( irrlicht ${LIBTYPE} ${IRRLICHT_SRC_FILES} ${IRRLICHT_LIB_FILES} )
#ADD_LIBRARY( irrlicht_shared SHARED ${IRRLICHT_SRC_FILES} ${IRRLICHT_LIB_FILES} )

IF( WIN32 )
    # Add windows specific properties if we are on windows.
	SET_TARGET_PROPERTIES( irrlicht PROPERTIES RUNTIME_OUTPUT_NAME irrlicht LINKER_LANGUAGE CXX )
ELSEIF( APPLE )
    # Add mac specific properties if we are on mac
	IF( XCODE_VERSION MATCHES ^4\\. )
		# To compile irrlicht on Xcode4, we need to use llvm gcc
		SET_TARGET_PROPERTIES( irrlicht PROPERTIES XCODE_ATTRIBUTE_GCC_VERSION "com.apple.compilers.llvmgcc42" )
		#SET_TARGET_PROPERTIES( irrlicht_shared PROPERTIES XCODE_ATTRIBUTE_GCC_VERSION "com.apple.compilers.llvmgcc42" )
	ENDIF(  )
ENDIF(  )

if( LIBTYPE MATCHES STATIC)
    SET_TARGET_PROPERTIES( irrlicht PROPERTIES LIBRARY_OUTPUT_NAME irrlicht ARCHIVE_OUTPUT_NAME irrlicht LINKER_LANGUAGE CXX )
else()
	TARGET_LINK_LIBRARIES( irrlicht ${IRRLICHT_LINK_LIBS} )
	install(TARGETS irrlicht DESTINATION ${CMAKE_INSTALL_PREFIX}) # irrlicht needs to go to the install dir on windows as it's a shared lib
endif()

################################################################################
# Export Irrlicht link libraries & include directory
IF( NOT (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR) )
	SET( IRRLICHT_LINK_LIBS ${IRRLICHT_LINK_LIBS} PARENT_SCOPE )
	SET( IRRLICHT_INC_DIR ${IRR_SOURCES} ${IRR_INCLUDES} PARENT_SCOPE )
ENDIF(  )
################################################################################
