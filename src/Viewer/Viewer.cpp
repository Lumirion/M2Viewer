#include <irrlicht.h>
#include <iostream>
#include "CM2MeshFileLoader.h"
#include "CM2MeshSceneNode.h"
#include "CImageLoaderBLP.h"

using namespace irr; 
using namespace core; 
using namespace video; 
using namespace scene;


#if defined(_MSC_VER)
#pragma comment(lib, "Irrlicht.lib")
#endif

int main() 
{   
	IrrlichtDevice* device = createDevice(EDT_OPENGL, dimension2d<u32>(640, 480), 16, false, false, false, 0);
	if (!device)
		return 1;    
	IVideoDriver* driver = device->getVideoDriver();
	ISceneManager* smgr = device->getSceneManager();
	
	// register external loader for m2 filetype
    smgr->addExternalMeshLoader(new scene::CM2MeshFileLoader(device));
	driver->addExternalImageLoader(new irr::video::CImageLoaderBLP());
	CM2MeshSceneNodeFactory *fact = new CM2MeshSceneNodeFactory(smgr);
	smgr->registerSceneNodeFactory(fact);
	
	// see if I got the directory right
	if (device->getFileSystem()->existFile("Data\\Interface\\GLUES\\MODELS\\UI_MainMenu_Northrend\\UI_MainMenu_Northrend.m2") == false)
	{
		std::cout << "Can't find the m2 file with this path.\n";
	}
	else
	{
		std::cout << "The example m2 was found in the Data folder.\n";
	}
	
	CM2Mesh* mesh = (CM2Mesh*)smgr->getMesh("Data\\Interface\\GLUES\\MODELS\\UI_MainMenu_Northrend\\UI_MainMenu_Northrend.m2"); //smgr->getMesh("sydny.md2");
	CM2MeshSceneNode* node = ((scene::CM2MeshSceneNode*)(fact->addM2SceneNode(mesh, NULL))); //new CM2MeshSceneNode(mesh, smgr->getRootSceneNode(), smgr, -1); // smgr->addAnimatedMeshSceneNode(mesh);
	node->setAnimationSpeed(1000);
	node->setM2Animation(0);
	
    ICameraSceneNode* cam = smgr->addCameraSceneNode(0,core::vector3df(11.11f,2.44f,-0.03f),core::vector3df(-10.28f,2.44f,-0.04f));
    cam->setFarValue(2777.7f);
    cam->setNearValue(0.222f);
	
	while (device->run())
	{
		driver->beginScene(true, true, SColor(255, 255, 255,255));
		smgr->drawAll();
		driver->endScene();
	}
	device->drop();
	return 0;
}